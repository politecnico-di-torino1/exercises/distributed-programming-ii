package it.polito.dp2.RNS.sol1;

import it.polito.dp2.RNS.RnsReader;
import it.polito.dp2.RNS.RnsReaderException;
import it.polito.dp2.RNS.sol1.impl.RnsReaderImpl;

public class RnsReaderFactory extends it.polito.dp2.RNS.RnsReaderFactory {
	
	@Override
	public RnsReader newRnsReader() throws RnsReaderException {
		return new RnsReaderImpl();
	}
	
	public static RnsReaderFactory newInstance() {
		return new RnsReaderFactory();
	}

}
