package it.polito.dp2.RNS.sol1.impl;

import it.polito.dp2.RNS.GateReader;
import it.polito.dp2.RNS.GateType;
import it.polito.dp2.RNS.sol1.jaxb.Place;

public class GateImpl extends PlaceImpl implements GateReader {
	private GateType type;
	
	public GateImpl(Place place) {
		super(place);
		type = GateType.valueOf(place.getGate().getType().value());
	}

	@Override
	public GateType getType() {
		return type;
	}

}
