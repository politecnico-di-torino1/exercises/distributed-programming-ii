package it.polito.dp2.RNS.sol1.impl;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import it.polito.dp2.RNS.ConnectionReader;
import it.polito.dp2.RNS.GateReader;
import it.polito.dp2.RNS.GateType;
import it.polito.dp2.RNS.ParkingAreaReader;
import it.polito.dp2.RNS.PlaceReader;
import it.polito.dp2.RNS.RnsReader;
import it.polito.dp2.RNS.RnsReaderException;
import it.polito.dp2.RNS.RoadSegmentReader;
import it.polito.dp2.RNS.VehicleReader;
import it.polito.dp2.RNS.VehicleState;
import it.polito.dp2.RNS.VehicleType;
import it.polito.dp2.RNS.sol1.jaxb.RoadNavigationSystem;

public class RnsReaderImpl implements RnsReader {
	private RoadNavigationSystem rns;
	private JAXBContext jc;
	private Unmarshaller u;
	private String filename;
	private Logger logger = Logger.getLogger("RnsReaderImpl");
	
	private Map<String, GateReader> gates = new HashMap<>();
	private Map<String, ParkingAreaReader> parkingAreas = new HashMap<>();
	private Map<String, RoadSegmentReader> roadSegments = new HashMap<>();
	private Map<String, PlaceReader> places = new HashMap<>();
	private Set<ConnectionReader> connections = new HashSet<>();
	private Map<String, VehicleReader> vehicles = new HashMap<>();
	
	public RnsReaderImpl() throws RnsReaderException {
		filename = System.getProperty("it.polito.dp2.RNS.sol1.RnsInfo.file");
		
		try {
			jc = JAXBContext.newInstance("it.polito.dp2.RNS.sol1.jaxb");
			u = jc.createUnmarshaller();
			
			SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
			try {
				Schema schema = sf.newSchema(new File("xsd/rnsInfo.xsd"));
				u.setSchema(schema);
				u.setEventHandler(new ValidationEventHandler() {
					
					@Override
					public boolean handleEvent(ValidationEvent ve) {
						if(ve.getSeverity() != ValidationEvent.WARNING) {
							ValidationEventLocator vel = ve.getLocator();
							System.err.println("Line:Col[" + vel.getLineNumber() +
									":" + vel.getColumnNumber() +
									"]" + ve.getMessage()
									);
						}
						return false;
					}
				});
			} catch(SAXException e) {
				logger.log(Level.WARNING, "Unexpected SAXException.");
				throw new RnsReaderException();
			}
			rns = (RoadNavigationSystem) u.unmarshal(new File(filename));
		} catch(JAXBException e) {
			logger.log(Level.WARNING, "Unexpected JAXBException.");
			throw new RnsReaderException();
		}
		
		// Read initial data
		loadInitialData();
	}
	
	private void loadInitialData() {
		readAllPlaces();
		buildPlaces();
		readConnections();
		readVehicles();
	}
	
	private void readConnections() {
		rns.getConnection().stream().forEach(c -> {
			PlaceReader from = places.get(c.getFrom()),
					to = places.get(c.getTo());
			
			Connectionmpl connection = new Connectionmpl(from, to);
			connections.add(connection);
			from.getNextPlaces().add(to);
		});
	}
	
	private void readAllPlaces() {
		gates = rns.getPlace().stream()
				.filter(p -> p.getGate() != null)				// Read only parking areas
				.collect(Collectors.toMap(
						g -> g.getId(),
						g -> new GateImpl(g)
				));

		parkingAreas = rns.getPlace().stream()
				.filter(p -> p.getParkingArea() != null)		// Read only parking areas
				.collect(Collectors.toMap(
						pa -> pa.getId(),
						pa -> new ParkingAreaImpl(pa)
				));

		roadSegments = rns.getPlace().stream()
				.filter(p -> p.getRoadSegment() != null)		// Read only parking areas
				.collect(Collectors.toMap(
						rs -> rs.getId(),
						rs -> new RoadSegmentImpl(rs)
				));
	}
	
	private void buildPlaces() {
		places.putAll(gates);
		places.putAll(parkingAreas);
		places.putAll(roadSegments);
	}
	
	private void readVehicles() {
		rns.getVehicle().stream().forEach(v -> {
			PlaceReader from = places.get(v.getOrigin()),
					to = places.get(v.getDestination()),
					position = places.get(v.getPosition());
			
			VehicleImpl vehicle = new VehicleImpl(v, from, to, position);
			vehicles.put(v.getId(), vehicle);
		});		
	}
	
	public Set<VehicleReader> getVehicles(Calendar since, Set<VehicleType> types, VehicleState state) {
		return vehicles.values().stream()
				.filter(v -> (types != null ? types.contains(v.getType()) : true))
				.filter(v -> (state != null ? v.getState().equals(state) : true))
				.filter(v -> (since != null ? v.getEntryTime().compareTo(since) > 0 : true))
				.collect(Collectors.toSet());
	}
	
	public VehicleReader getVehicle(String id) {
		return vehicles.get(id);
	}
	
	public Set<RoadSegmentReader> getRoadSegments(String roadName) {
		return roadSegments.values().stream()
				.filter(r -> (roadName != null ? r.getRoadName().equals(roadName) : true))
				.collect(Collectors.toSet());
	}
	
	public Set<PlaceReader> getPlaces(String idPrefix) {
		return places.values().stream()
				.filter(p -> (idPrefix != null ? p.getId().startsWith(idPrefix) : true))
				.collect(Collectors.toSet());
	}
	
	public PlaceReader getPlace(String id) {
		return places.get(id);
	}
	
	public Set<ParkingAreaReader> getParkingAreas(Set<String> services) {
		return parkingAreas.values().stream()
				.filter(pa -> (services != null ? pa.getServices().containsAll(services) : true))
				.collect(Collectors.toSet());
	}
	
	public Set<GateReader> getGates(GateType type) {
		return gates.values().stream()
				.filter(g-> (type != null ? g.getType().equals(type.toString()) : true))
				.collect(Collectors.toSet());
	}
	
	public Set<ConnectionReader> getConnections() {
		return connections;
	}
	
}
