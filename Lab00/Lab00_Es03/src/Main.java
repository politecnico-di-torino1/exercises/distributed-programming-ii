import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Main {

	static Set<String> words = new TreeSet<>();
	
	public static void main(String[] args) {
		if(args.length < 3) {
			System.err.println("Correct usage: <input_file1> <input_file2> <output_file>");
			System.exit(1);
		}
		
		try {
			readFile(args[0], words);
			readFile(args[1], words);
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open files");
		} catch (IOException e) {
			System.out.println("Error reading files");
		}
		
		try {
			writeFile(args[2], words);
		} catch (IOException e) {
			System.out.println("Error writing file '" + args[2] + '"');
		}
	}
	
	private static void readFile(String filename, Set<String> set) throws FileNotFoundException, IOException {
		try (BufferedReader r = new BufferedReader(new FileReader(filename))){
            r.lines()
             	.collect(Collectors.toCollection(() -> set));
		}
	}

	private static void writeFile(String filename, Set<String> set) throws IOException {
		try (BufferedWriter w = new BufferedWriter(new FileWriter(filename))) {
            for(String s : set) {
            	w.write(s + "\n");
            }
		}
	}
}
