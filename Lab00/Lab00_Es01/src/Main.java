import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class Main {

	static String dictionaryFile = "dictionary.txt",
			inputFile;
	static Collection<String> dictionary = new HashSet<>(),
			input = new HashSet<>();
	
	public static void main(String[] args) {
		if(args.length < 1) {
			System.err.println("Correct usage: <input_file>");
			System.exit(1);
		}
		inputFile = args[0];
		
		try {
			readFile(dictionaryFile, dictionary);
			readFile(inputFile, input);
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open files");
		} catch (IOException e) {
			System.out.println("Error reading files");
		}
		
		input.stream()
			.filter(l -> ! dictionary.contains(l))
			.forEach(System.out::println);
	}
	
	private static void readFile(String path, Collection<String> collection) throws FileNotFoundException, IOException {
		try(BufferedReader r = new BufferedReader(new FileReader(path))) {
			r.lines()
//				.peek(System.out::println)
				.collect(Collectors.toCollection(() -> collection));
		}
	}

}
