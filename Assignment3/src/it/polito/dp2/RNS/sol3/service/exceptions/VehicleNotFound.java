package it.polito.dp2.RNS.sol3.service.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class VehicleNotFound extends ClientErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8002492221260210467L;
	private static final int CODE = ExceptionCodes.VEHICLE_NOT_FOUND;
	
	public VehicleNotFound() {
		super(CODE);
	}

	public VehicleNotFound(String message) {
		super(Response.status(CODE).entity(message).build());
	}

}
