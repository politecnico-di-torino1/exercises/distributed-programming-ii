package it.polito.dp2.RNS.sol3.service.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class PlaceNotFound extends ClientErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2870679228545628000L;
	private static final int CODE = ExceptionCodes.PLACE_NOT_FOUND;
	
	public PlaceNotFound() {
		super(CODE);
	}

	public PlaceNotFound(String message) {
		super(Response.status(CODE).entity(message).build());
	}

}
