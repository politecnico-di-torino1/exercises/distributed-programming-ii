package it.polito.dp2.RNS.sol3.service.exceptions;

public final class ExceptionCodes {
	// 409 - HTTP Reference
	public static final int CONFLICT = 409;
	
	// Custom exceptions
	public static final int INVALID_GATE = 461;
	public static final int PLACE_NOT_FOUND = 462;
	public static final int GATE_NOT_FOUND = 463;
	public static final int PATH_NOT_FOUND = 464;
	public static final int VEHICLE_NOT_FOUND = 465;
}
