package it.polito.dp2.RNS.sol3.service.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class GateNotFound extends ClientErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3514653107436116015L;
	private static final int CODE = ExceptionCodes.GATE_NOT_FOUND;
	
	public GateNotFound() {
		super(CODE);
	}

	public GateNotFound(String message) {
		super(Response.status(CODE).entity(message).build());
	}

}
