package it.polito.dp2.RNS.sol3.service.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class InvalidGateException extends ClientErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1223071468164607681L;
	private static final int CODE = ExceptionCodes.INVALID_GATE;
	
	public InvalidGateException() {
		super(CODE);
	}
	
	public InvalidGateException(String message) {
		super(Response.status(CODE).entity(message).build());
	}

}
