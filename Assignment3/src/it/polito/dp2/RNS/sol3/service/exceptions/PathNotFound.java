package it.polito.dp2.RNS.sol3.service.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class PathNotFound extends ClientErrorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -242395718672720075L;
	private static final int CODE = ExceptionCodes.PATH_NOT_FOUND;
	
	public PathNotFound() {
		super(CODE);
	}

	public PathNotFound(String message) {
		super(Response.status(CODE).entity(message).build());
	}

}
