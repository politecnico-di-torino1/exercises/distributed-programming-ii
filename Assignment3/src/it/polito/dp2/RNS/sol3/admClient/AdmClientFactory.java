package it.polito.dp2.RNS.sol3.admClient;

import it.polito.dp2.RNS.lab3.AdmClient;
import it.polito.dp2.RNS.lab3.AdmClientException;

public class AdmClientFactory extends it.polito.dp2.RNS.lab3.AdmClientFactory {

	@Override
	public AdmClient newAdmClient() throws AdmClientException {
		try {
			return new AdmClientImpl();
		} catch(ServerException e) {
			System.err.println(e.getMessage());
			throw new AdmClientException("AdmClient implementation is not available or cannot be instantiated.");
		} catch(Exception e) {
			System.err.println(e.getMessage());
			throw new AdmClientException(e.getMessage());
		}
	}

}
