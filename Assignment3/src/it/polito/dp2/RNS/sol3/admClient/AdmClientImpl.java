package it.polito.dp2.RNS.sol3.admClient;

import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import it.polito.dp2.RNS.ConnectionReader;
import it.polito.dp2.RNS.GateReader;
import it.polito.dp2.RNS.GateType;
import it.polito.dp2.RNS.ParkingAreaReader;
import it.polito.dp2.RNS.PlaceReader;
import it.polito.dp2.RNS.RoadSegmentReader;
import it.polito.dp2.RNS.VehicleReader;
import it.polito.dp2.RNS.VehicleState;
import it.polito.dp2.RNS.VehicleType;
import it.polito.dp2.RNS.lab3.AdmClient;
import it.polito.dp2.RNS.lab3.ServiceException;
import it.polito.dp2.RNS.sol1.impl.Connectionmpl;
import it.polito.dp2.RNS.sol1.impl.GateImpl;
import it.polito.dp2.RNS.sol1.impl.ParkingAreaImpl;
import it.polito.dp2.RNS.sol1.impl.RoadSegmentImpl;
import it.polito.dp2.RNS.sol1.impl.VehicleImpl;
import it.polito.dp2.RNS.sol3.admClient.jaxb.rnsSystem.Connections;
import it.polito.dp2.RNS.sol3.admClient.jaxb.rnsSystem.Places;
import it.polito.dp2.RNS.sol3.admClient.jaxb.rnsSystem.Vehicle;
import it.polito.dp2.RNS.sol3.admClient.jaxb.rnsSystem.Vehicles;

public class AdmClientImpl implements AdmClient {

	private Client client;
	private WebTarget target;
	private Map<String, GateReader> gates;
	private Map<String, ParkingAreaReader> parkingAreas;
	private Map<String, RoadSegmentReader> roadSegments;
	private Map<String, PlaceReader> places;
	private Set<ConnectionReader> connections;
	
	public AdmClientImpl() throws ServerException {
		client = ClientBuilder.newClient();
		target = client.target(this.getBaseURI());
		
		gates = new HashMap<>();
		parkingAreas = new HashMap<>();
		roadSegments = new HashMap<>();
		places = new HashMap<>();
		connections = new HashSet<>();
		
		loadInitialData();
	}
	
	private void loadInitialData() throws ServerException {
		readPlaces();
		buildPlaces();
		readConnections();
	}

	private void readPlaces() throws ServerException {
		Response response = target.path("places")
				.request()
				.header("admin", true)
				.accept(MediaType.APPLICATION_JSON)
				.get();
		if(response.getStatus() != 200) {
			String msg = "[Admin]\tError reading places: "
					+ response.getStatus()
					+ " " + response.getStatusInfo();
			System.err.println(msg);
			throw new ServerException(msg);
		}
		response.bufferEntity();
		Places gatesRead = response.readEntity(Places.class);

		gates = gatesRead.getPlace().stream()
				.filter(p -> p.getGate() != null)				// Read only parking areas
				.collect(Collectors.toMap(
						g -> g.getId(),
						g -> new GateImpl(g)
				));

		parkingAreas = gatesRead.getPlace().stream()
				.filter(p -> p.getParkingArea() != null)		// Read only parking areas
				.collect(Collectors.toMap(
						pa -> pa.getId(),
						pa -> new ParkingAreaImpl(pa)
				));

		roadSegments = gatesRead.getPlace().stream()
				.filter(p -> p.getRoadSegment() != null)		// Read only road segments
				.collect(Collectors.toMap(
						rs -> rs.getId(),
						rs -> new RoadSegmentImpl(rs)
				));
	}

	private void readConnections() throws ServerException {
		Response response = target.path("connections")
				.request()
				.header("admin", true)
				.accept(MediaType.APPLICATION_JSON)
				.get();
		if(response.getStatus() != 200) {
			String msg = "[Admin]\tError reading connections: "
					+ response.getStatus()
					+ " " + response.getStatusInfo();
			System.err.println(msg);
			throw new ServerException(msg);
		}
		response.bufferEntity();
		Connections connectionsRead = response.readEntity(Connections.class);
		
		connectionsRead.getConnection().stream().forEach(c -> {
			PlaceReader from = places.get(c.getFrom().getId()),
					to = places.get(c.getTo().getId());
			
			Connectionmpl connection = new Connectionmpl(from, to);
			connections.add(connection);
			from.getNextPlaces().add(to);
		});
	}
		
	@Override
	public Set<ConnectionReader> getConnections() {
		System.out.println("[Admin]\tReturning connections");
		connections.stream()
				.peek(c -> System.out.println("[Admin]\tRetrieved connection " + c.getFrom().getId() + " - " + c.getTo().getId()));
		return connections;
	}

	@Override
	public Set<GateReader> getGates(GateType type) {
		System.out.println("[Admin]\tReturning gates");
		return gates.values().stream()
				.filter(g -> (type != null ? g.getType().equals(type) : false))
				.peek(g -> System.out.println("[Admin]\tRetrieved gate " + g.getId()))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ParkingAreaReader> getParkingAreas(Set<String> services) {
		System.out.println("[Admin]\tReturning parking areas");
		return parkingAreas.values().stream()
				.filter(pa -> (services != null ? pa.getServices().containsAll(services) : true))
				.peek(pa -> System.out.println("[Admin]\tRetrieved parking area " + pa.getId()))
				.collect(Collectors.toSet());
	}

	@Override
	public PlaceReader getPlace(String id) {
		System.out.println("[Admin]\tReturning place " + id);
		return places.get(id);
	}

	@Override
	public Set<PlaceReader> getPlaces(String idPrefix) {
		System.out.println("[Admin]\tReturning places starting with " + idPrefix);
		return places.values().stream()
				.filter(p -> (idPrefix != null ? p.getId().startsWith(idPrefix) : true))
				.peek(p -> System.out.println("[Admin]\tRetrieved place " + p.getId()))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<RoadSegmentReader> getRoadSegments(String roadName) {
		System.out.println("[Admin]\tReturning road segments " + roadName);
		return roadSegments.values().stream()
				.filter(r -> (roadName != null ? r.getRoadName().equals(roadName) : true))
				.peek(r -> System.out.println("[Admin]\tRetrieved road segment " + r.getId()))
				.collect(Collectors.toSet());
	}
	
	/* Not implemented */
	@Override
	public VehicleReader getVehicle(String id) {
		return null;
	}

	/* Not implemented */
	@Override
	public Set<VehicleReader> getVehicles(Calendar since, Set<VehicleType> types, VehicleState state) {
		return null;
	}

	@Override
	public Set<VehicleReader> getUpdatedVehicles(String placeId) throws ServiceException {
		System.out.println("[Admin]\tReading vehicles in place " + placeId);
		Response response = target.path("vehicles")
				.queryParam("placeId", placeId)
				.request()
				.header("admin", true)
				.accept(MediaType.APPLICATION_JSON)
				.get();
		if(response.getStatus() != 200) {
			String msg = "Error reading vehicles from place: "
					+ response.getStatus()
					+ " " + response.getStatusInfo();
			System.out.println(msg);
			throw new ServiceException(msg);
		}
		response.bufferEntity();
		
		Vehicles vehiclesRead = response.readEntity(Vehicles.class);
		return vehiclesRead.getVehicle().stream()
				.map(v -> getVehicleImpl(v))
				.peek(v -> System.out.println("[Admin]\tRetrieved vehicle " + v.getId()))
				.collect(Collectors.toSet());
	}
	
	@Override
	public VehicleReader getUpdatedVehicle(String id) throws ServiceException {
		System.out.println("[Admin]\tReading vehicle " + id);
		Response response = target.path("vehicles").path(id)
				.request()
				.header("admin", true)
				.accept(MediaType.APPLICATION_JSON)
				.get();
		if(response.getStatus() == 404) {
			System.out.println("Vehicle " + id + " not found. "
					+ response.getStatus()
					+ " " + response.getStatusInfo());
			return null;
		}
		if(response.getStatus() != 200) {
			String msg = "Error reading vehicle " + id + ": "
					+ response.getStatus()
					+ " " + response.getStatusInfo();
			System.out.println(msg);
			throw new ServiceException(msg);
		}
		response.bufferEntity();
		
		Vehicle vehicleRead = response.readEntity(Vehicle.class);
		PlaceReader origin = places.get(vehicleRead.getOrigin().getId()),
				destination = places.get(vehicleRead.getDestination().getId()),
				position = places.get(vehicleRead.getPosition().getId());
		VehicleImpl vehicle = new VehicleImpl(vehicleRead, origin, destination, position);
		
		return vehicle;
	}
	
	// Utilities

	private URI getBaseURI() {
		String baseURI = System.getProperty("it.polito.dp2.RNS.lab3.URL");
		if(baseURI == null) // Property not set, use default value
			baseURI = "http://localhost:8080/RnsSystem/rest";
		
		return UriBuilder.fromUri(baseURI)
				.path("webapi")
				.build();
	}
	
	private void buildPlaces() {
		places.putAll(gates);
		places.putAll(parkingAreas);
		places.putAll(roadSegments);
	}

	private VehicleImpl getVehicleImpl(Vehicle v) {
		PlaceReader origin = places.get(v.getOrigin().getId()),
				destination = places.get(v.getDestination().getId()),
				position = places.get(v.getPosition().getId());
		return new VehicleImpl(v, origin, destination, position);
	}
	
}
