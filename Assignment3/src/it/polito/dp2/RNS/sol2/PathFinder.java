package it.polito.dp2.RNS.sol2;

import java.math.BigInteger;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import it.polito.dp2.RNS.ConnectionReader;
import it.polito.dp2.RNS.PlaceReader;
import it.polito.dp2.RNS.RnsReader;
import it.polito.dp2.RNS.lab2.BadStateException;
import it.polito.dp2.RNS.lab2.ModelException;
import it.polito.dp2.RNS.lab2.ServiceException;
import it.polito.dp2.RNS.lab2.UnknownIdException;
import it.polito.dp2.RNS.sol2.jaxb.neo4j.GetShortestPath;
import it.polito.dp2.RNS.sol2.jaxb.neo4j.PostNode;
import it.polito.dp2.RNS.sol2.jaxb.neo4j.PostRelationship;
import it.polito.dp2.RNS.sol2.jaxb.neo4j.ResponseShortestPath;

public class PathFinder implements it.polito.dp2.RNS.lab2.PathFinder {
	private RnsReader monitor;
	private Client client;
	private WebTarget target;
	private Map<String, URI> placesURI = new HashMap<>();
	private Map<URI, String> URIplaces = new HashMap<>();
	private Set<URI> connectionsURI = new HashSet<>();
	private boolean modelLoaded;
	
	private final static String RELATIONSHIP_TYPE = "ConnectedTo";
	private final static String ALGORITHM = "shortestPath";
	private final static String DIRECTION = "out";
	
	public PathFinder(RnsReader monitor, String baseURI) {
		this.monitor = monitor;
		this.client = ClientBuilder.newClient();
		this.target = client.target(this.getBaseURI(baseURI));
		this.modelLoaded = false;
	}
	
	@Override
	public boolean isModelLoaded() {
		return this.modelLoaded;
	}

	@Override
	public void reloadModel() throws ServiceException, ModelException {
		clearDatabase();

		Set<PlaceReader> places = monitor.getPlaces(null);
		Set<ConnectionReader> connections = monitor.getConnections();

    	// Post places
    	for(PlaceReader p : places) {
    		PostNode pn = new PostNode();
    		pn.setId(p.getId());
    		Response response = target
    				.request()
    				.accept(MediaType.APPLICATION_JSON)
    				.post(Entity.entity(pn, MediaType.APPLICATION_JSON));
    		if(response.getStatus() != 201) {
    			String msg = "Error occurred for place " + p.getId() + ".";
    			System.err.println(msg);
    			throw new ServiceException(msg);
			}
    		placesURI.put(p.getId(), response.getLocation());
			URIplaces.put(response.getLocation(), p.getId());
			response.close();
    	}
    	
    	// Post connections
    	for(ConnectionReader c : connections) {
    		PostRelationship pr = new PostRelationship();
    		URI toURI = placesURI.get(c.getTo().getId());
    		WebTarget fromTarget = client
    				.target(placesURI.get(c.getFrom().getId()))
    				.path("relationships");
    		pr.setTo(toURI.toString());
    		pr.setType(RELATIONSHIP_TYPE);
    		Response response = fromTarget
    				.request()
    				.accept(MediaType.APPLICATION_JSON)
    				.post(Entity.entity(pr, MediaType.APPLICATION_JSON));
    		if(response.getStatus() != 201) {
    			String msg = "Error occurred for connection " + c.getFrom().getId() + " -> " + c.getTo().getId() + ".";
    			System.err.println(msg);
    			throw new ServiceException(msg);
			}
			connectionsURI.add(response.getLocation());
			response.close();
    	}
    	
    	this.modelLoaded = true;
	}

	@Override
	public Set<List<String>> findShortestPaths(String source, String destination, int maxLength)
			throws UnknownIdException, BadStateException, ServiceException {
		// Check parameters
		if(!modelLoaded)
			throw new BadStateException("Model not loaded");
		if(!placesURI.containsKey(source))
			throw new UnknownIdException("Unrecognized source " + source);
		if(!placesURI.containsKey(destination))
			throw new UnknownIdException("Unrecognized destination " + destination);
		
		// Prepare request
		GetShortestPath gsp = new GetShortestPath();
		gsp.setTo(placesURI.get(destination).toString());
		gsp.setMaxDepth(BigInteger.valueOf(maxLength));
		gsp.setAlgorithm(ALGORITHM);
		gsp.setRelationships(new GetShortestPath.Relationships());
		gsp.getRelationships().setType(RELATIONSHIP_TYPE);
		gsp.getRelationships().setDirection(DIRECTION);
		
		WebTarget target = client.target(placesURI.get(source)).path("paths");
		Response response = target.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(gsp, MediaType.APPLICATION_JSON));
		if(response.getStatus() != 200) {
			throw new ServiceException("Error in remote operation: "
					+ response.getStatus()
					+ " " + response.getStatusInfo());
		}
		response.bufferEntity();
		List<ResponseShortestPath> shortestPaths = response.readEntity(new GenericType<List<ResponseShortestPath>>(){});

		return shortestPaths.stream()
				.map(sp -> sp.getNodes().stream()
						.map(n -> URIplaces.get(URI.create(n))) // Find corresponding ID
						.collect(Collectors.toList()) // List<String> - List of IDs
				)
				.collect(Collectors.toSet()); // Set<List<String>>
	}
	
	private void clearDatabase() {
		modelLoaded = false;
		
		// Delete relationships
		connectionsURI.stream().forEach(r -> {
			WebTarget target = client.target(r);
			Response response = target.request(MediaType.APPLICATION_JSON)
					.delete();
			if(response.getStatus() != 204) {
				System.err.println("Error in remote operation: "
					+ response.getStatus()
					+ " " + response.getStatusInfo());
			}
		});
		
		// Delete nodes
		URIplaces.keySet().stream().forEach(n -> {
			WebTarget target = client.target(n);
			Response response = target.request(MediaType.APPLICATION_JSON)
					.delete();
			if(response.getStatus() != 204) {
				System.err.println("Error in remote operation: "
					+ response.getStatus()
					+ " " + response.getStatusInfo());
			}
		});
		
		connectionsURI.clear();
		placesURI.clear();
		URIplaces.clear();
	}
	
	private URI getBaseURI(String baseURI) {
		return UriBuilder.fromUri(baseURI)
				.path("data")
				.path("node")
				.build();
	}

}
