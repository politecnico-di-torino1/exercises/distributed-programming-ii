package it.polito.dp2.RNS.sol1.impl;

import it.polito.dp2.RNS.RoadSegmentReader;
import it.polito.dp2.RNS.sol3.admClient.jaxb.rnsSystem.Place;

public class RoadSegmentImpl extends PlaceImpl implements RoadSegmentReader {
	private String roadName;
	private String name;
	
	public RoadSegmentImpl(Place place) {
		super(place);
		this.name = place.getRoadSegment().getName();
		this.roadName = place.getRoadSegment().getRoadName();
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getRoadName() {
		return roadName;
	}

}
