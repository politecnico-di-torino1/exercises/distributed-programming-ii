package it.polito.dp2.RNS.sol2;

import it.polito.dp2.RNS.FactoryConfigurationError;
import it.polito.dp2.RNS.RnsReaderException;
import it.polito.dp2.RNS.RnsReaderFactory;
import it.polito.dp2.RNS.lab2.PathFinderException;
import it.polito.dp2.RNS.sol2.PathFinder;

public class PathFinderFactory extends it.polito.dp2.RNS.lab2.PathFinderFactory {
	private static final String PROPERTY = "it.polito.dp2.RNS.lab2.URL";
	private static final String BASE_URI = System.getProperty(PROPERTY);
	
	@Override
	public PathFinder newPathFinder() throws PathFinderException {
		if(BASE_URI == null)
			throw new PathFinderException("Error reading property " + PROPERTY);
		
		try {
			return new PathFinder(RnsReaderFactory.newInstance().newRnsReader(), BASE_URI);
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
			throw new PathFinderException();
		} catch (RnsReaderException e) {
			e.printStackTrace();
			throw new PathFinderException();
		}
	}
	
	public static PathFinderFactory newInstance() {
		return new PathFinderFactory();
	}

}
